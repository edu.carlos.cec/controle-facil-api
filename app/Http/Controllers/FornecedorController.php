<?php

namespace App\Http\Controllers;

use App\Models\Fornecedor;
use Illuminate\Http\Request;

class FornecedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Procurar por todas as empresas
        $fornecedores = Fornecedor::all();

        if(!empty($fornecedores->all())){
            return response()->json($fornecedores->all());
        } else {
            return response()->json(['mensagem' => 'Não há registros.']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //PEGANDO OS VALORES DA REQUISIÇÃO
        $req = $request->all();

        //INSERINDO OS DADOS DO REQUEST
        $fornecedor = Fornecedor::create($req);

        //RETORNANDO OS DADOS DO INSERT
        return response()->json($fornecedor);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fornecedor = Fornecedor::where('id_fornecedor' , $id)->first();

        if(!empty($fornecedor)){
            return response()->json($fornecedor);
        } else {
            return response()->json(['mensagem' => 'Nenhum fornecedor com esse id.']);
        }
    }

    /**
     * Display the providers of an enterprise
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function providersEnterprise($idEnterprise){

        $fornecedores = Fornecedor::where('id_empresa' , $idEnterprise)->get();
        if(!empty($fornecedores->all())){
            return response()->json($fornecedores->all());
        } else {
            return response()->json(['mensagem' => 'Nenhum fornecedor com esse id.']);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //PEGAR OS DADOS DA REQUISIÇÃO
        $req = $request->all();

        //ALTERANDO OS DADOS
        $fornecedor = Fornecedor::where('id_fornecedor' , $id);

        if(!empty($fornecedor->get())){

            //UPDATE COM OS DADOS DA REQUISIÇÃO
            $update = $fornecedor->update($req);


            //RETORNANDO OS DADOS
            if($update){
                return response()->json(["mensagem" => "Update feito com sucesso!"]);
            } else {
                return response()->json(["mensagem" => "Problema com o update!"]);
            }
        } else {
            return response()->json(["mensagem" => "Fornecedor não existe!"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //PEGAR O ID PASSADO
        $delete = Fornecedor::where('id_fornecedor', $id)->delete();

        if($delete){
            return response()->json(['mensagem' => 'Fornecedor removido com sucesso!']);
        } else {
            return response()->json(['mensagem' => 'Erro ao remover o fornecedor!']);
        }
    }


    public function findTotalProvider(){

        $fornecedores = Fornecedor::count();
        if($fornecedores){
            return response()->json(["total" => $fornecedores]);
        } else {
            return response()->json(['mensagem' => 'Problema com o select!']);
        }
    }


}
