<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Empresas;
use Illuminate\Support\Facades\DB;

class EmpresasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Procurar por todas as empresas
        $empresas = Empresas::all();

        if(!empty($empresas->all())){
            return response()->json($empresas->all());
        } else {
            return response()->json(['mensagem' => 'Não há registros.']);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //PEGANDO OS VALORES DA REQUISIÇÃO
        $req = $request->all();

        //INSERINDO OS DADOS DO REQUEST
        $empresa = Empresas::create($req);

        //RETORNANDO OS DADOS DO INSERT
        return response()->json($empresa);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresas::where('id_empresa' , $id)->first();
        if(!empty($empresa->all())){
            return response()->json($empresa);
        } else {
            return response()->json(['mensagem' => 'Nenhuma empresa com esse id.']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //PEGAR OS DADOS DA REQUISIÇÃO
        $req = $request->all();

        //ALTERANDO OS DADOS
        $empresa = Empresas::where('id_empresa' , $id);

        if(!empty($empresa->get())){

            //UPDATE COM OS DADOS DA REQUISIÇÃO
            $update = $empresa->update($req);

            //RETORNANDO OS DADOS
            if($update){
                return response()->json(["mensagem" => "Update feito com sucesso!"]);
            } else {
                return response()->json(["mensagem" => "Problema com o update!"]);
            }
        } else {
            return response()->json(["mensagem" => "Empresa não existe!"]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //PEGAR O ID PASSADO
        $delete = Empresas::where('id_empresa', $id)->delete();

        if($delete){
            return response()->json(['mensagem' => 'Empresa removida com sucesso!']);
        } else {
            return response()->json(['mensagem' => 'Problema ao remover a empresa!']);
        }
    }
}
