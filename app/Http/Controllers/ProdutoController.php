<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produtos;
use Illuminate\Database\QueryException;

class ProdutoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //PROCURAR TODOS OS PRODUTOS
        $produtos = Produtos::all();

        //VERIFICAER SE PRODUTO ESTÁ COM ITENS
        if(!empty($produtos->all())){
            return response()->json($produtos->all());
        } else {
            return response()->json(['mensagem' => 'Não há registros.']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try{

            //PEGANDO OS VALORES DA REQUISIÇÃO
            $req = $request->all();

            //CRIANDO O PRODUTO
            $produto = Produtos::create($req);

            //RETORNANDO OS DADOS DO INSERT
            return response()->json($produto);

        } catch(QueryException $qe) {

            //RETORNANDO OS DADOS DO INSERT
            return response()->json(['success' => false, 'mensagem' => 'Problema na inserção do produto!']);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //SELECIONANDO UM PRODUTO ESPECÍFICO
        $produto = Produtos::where('id_produto' , $id)->first();

        if(!empty($produto->all())){
            return response()->json($produto);
        } else {
            return response()->json(['mensagem' => 'Nenhum produto com esse id.']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        //PEGAR OS DADOS DA REQUISIÇÃO
        $req = $request->all();

        //ALTERANDO OS DADOS
        $produto = Produtos::where('id_produto' , $id);

        if(!empty($produto->get())){

            //UPDATE COM OS DADOS DA REQUISIÇÃO
            $update = $produto->update($req);

            //RETORNANDO OS DADOS
            if($update){
                return response()->json(["mensagem" => "Update feito com sucesso!"]);
            } else {
                return response()->json(["mensagem" => "Problema com o update!"]);
            }
        } else {
            return response()->json(["mensagem" => "Produto não existe!"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //PEGAR O ID PASSADO
        $delete = Produtos::where('id_produto', $id)->delete();

        if($delete){
            return response()->json(['mensagem' => 'Produto removido com sucesso!']);
        } else {
            return response()->json(['mensagem' => 'Problema ao remover o produto!']);
        }
    }

    public function findProductByProvider($id_empresa){

        $produtos = Produtos::where('id_empresa', $id_empresa)->get();

        if($produtos){
            return response()->json($produtos->all());
        } else {
            return response()->json(['mensagem' => 'Problema com o select!']);
        }
    }


    public function findTotalProductByProvider($id_empresa){
        $produtos = Produtos::where('id_empresa', $id_empresa)->count();
        if($produtos){
            return response()->json(["total" => $produtos]);
        } else {
            return response()->json(['mensagem' => 'Problema com o select!']);
        }
    }
}
