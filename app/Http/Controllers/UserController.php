<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{

    public function __construct(Request $request){
        $this->request = $request;
    }

    //Index dos usuários

    public function index(){
        //Fazer a lógica para pegar todos os usuários do banco de dados
        $usuarios = User::all();
        return response()->json($usuarios);
    }

    /*public function create(){
        //Pegamos a requisicao para fazer a inserção dos dados
        $request = $this->request;
        $request["csrf_token"] = csrf_token();
        //dd($request->all());
            return redirect("insert-user")->with('request', $request);
    }*/

    //Método para fazer a inserção de usuários
    public function store(){
        //echo "Deu certo criacao";
        dd($this->request->all());
    }


    public function show(){
        echo "mostrando somente um";
    }


    public function edit(){
        echo "Editando somente um";
    }

    public function update($id){
        //PEGAR OS DADOS DA REQUISIÇÃO
        $req = $this->request->all();

        //ALTERANDO OS DADOS
        $usuario = User::where('id_usuario' , $id);

        if(!empty($usuario->get())){
            //UPDATE COM OS DADOS DA REQUISIÇÃO
            $update = $usuario->update($req);

            //RETORNANDO OS DADOS
            if($update){
                return response()->json(["mensagem" => "Update feito com sucesso!"]);
            } else {
                return response()->json(["mensagem" => "Problema com o update!"]);
            }
        } else {
            return response()->json(["mensagem" => "Usuário não existe!"]);
        }
    }


    public function destroy(){
        echo "Removendo somente um";
    }
}
