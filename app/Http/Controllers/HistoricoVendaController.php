<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\HistoricoVenda;


class HistoricoVendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //PEGAR TODAS AS VENDAS
        $vendas = HistoricoVenda::all();

        if(!empty($vendas->all())){
            return response()->json($vendas->all());
        } else {
            return response()->json(['mensagem' => 'Não há registros.']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //PEGANDO OS VALORES DA REQUISIÇÃO
        $req = $request->all();

        //INSERINDO OS DADOS DO REQUEST
        $venda = HistoricoVenda::create($req);

        //RETORNANDO OS DADOS DO INSERT
        return response()->json($venda);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function totalAmountSell(){
        $totalAmountSell = HistoricoVenda::sum('preco_total');
        return response()->json(["total" => $totalAmountSell]);
    }

    public function totalSell(){
        $totalSell = HistoricoVenda::count();
        return response()->json(["total" => $totalSell]);
    }

    public function totalProductSell(){
        $totalSell = HistoricoVenda::sum('qtd_venda');
        return response()->json(["total" => $totalSell]);
    }
}
