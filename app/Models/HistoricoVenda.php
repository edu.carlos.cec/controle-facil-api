<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoricoVenda extends Model
{
    use HasFactory;

    protected $table = "historico_vendas";
    protected $primaryKey = "id_venda";

    protected $fillable = [
        'id_produto',
        'qtd_venda',
        'preco_total',
        'created_at',
        'updated_at'
    ];
}
