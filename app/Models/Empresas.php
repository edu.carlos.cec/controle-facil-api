<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresas extends Model
{
    use HasFactory;

    //Tabela
    protected $table = 'empresas';

    //DEFINE QUAIS CAMPOS DA NOSSA TABELA SERÁ AFETADA PELO INSERT
    protected $fillable = [
        'nome_empresa',
        'cnpj'
    ];
}
