<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
    use HasFactory;

    protected $table = "produtos";
    protected $primaryKey = "id_produto";

    protected $fillable = [
        'id_produto',
        'id_empresa',
        'id_fornecedor',
        'nome_produto',
        'preco',
        'qtd_disponivel',
        'status',
    ];
}
