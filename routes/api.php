<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpresasController;
use App\Http\Controllers\FornecedorController;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HistoricoVendaController;
use App\Http\Middleware\apiProtectedRoute;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Login ================================================================================================================
Route::post('auth/login', [AuthController::class , 'login']);
//======================================================================================================================

Route::post('refresh', 'AuthController@refresh');

//Achar o usuario logado ===============================================================================================
Route::post('me', [AuthController::class , 'me']);
//======================================================================================================================


//======================================================================================================================
//==================================                ADMIN               ================================================
//======================================================================================================================

Route::group(['middleware' => ['apiJwt']], function(){

    //Logout
    Route::post('auth/logout', [AuthController::class , 'logout']);

    //==================================================================================================================
    //Rota para Usuário ================================================================================================
    //==================================================================================================================
    Route::prefix('usuarios')->group(function(){

        //Mostrar todos os usuários
        Route::get("/", [UserController::class , "index"]);

        //Inserir um usuário
        Route::post("/", [UserController::class,  "store"]);

        //Pegar um usuário
        Route::get("/usuario/{id}", [UserController::class,  "show"]);

        //Editando um usuário
        Route::post("/usuario/{id}/edit", [UserController::class,  "edit"]);

        //Editando um usuario
        Route::put("/usuario/{id}", [UserController::class,  "update"]);

        //Deletando um usuario
        Route::delete("/usuario/{id}", [UserController::class,  "destroy"]);

    });
    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================



    //==================================================================================================================
    //Rota para Empresas ===============================================================================================
    //==================================================================================================================
    Route::prefix('empresas')->group(function(){

        //Mostrar todos as empresas
        Route::get("/", [EmpresasController::class , "index"]);

        //Inserir uma empresa
        Route::post("/", [EmpresasController::class,  "store"]);

        //Pegar uma empresa específica
        Route::get("/empresa/{id}", [EmpresasController::class,  "show"]);

        //Editando uma empresa
        Route::put("/empresa/{id}", [EmpresasController::class,  "update"]);

        //Deletando uma empresa
        Route::delete("/empresa/{id}", [EmpresasController::class,  "destroy"]);

    });
    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    //==================================================================================================================
    //Rota para Produtos ===============================================================================================
    //==================================================================================================================

    Route::prefix('produtos')->group(function(){

        //Rota inicial produto
        Route::get('/', [ProdutoController::class, 'index']);

        //Rota para criar um produto
        Route::get('/create', [ProdutoController::class, 'create']);

        Route::post('/', [ProdutoController::class, 'store']);

        Route::get('/{id}', [ProdutoController::class, 'show']);

        Route::get('/{id}/edit', [ProdutoController::class, 'edit']);

        Route::put('/{id}', [ProdutoController::class, 'update']);

        Route::delete('/{id}', [ProdutoController::class, 'destroy']);

        //Achar os produtos de uma empresa
        Route::get('/empresa/{id_empresa}', [ProdutoController::class, 'findProductByProvider']);

        //Total de produtos de uma empresa
        Route::get('/empresa/{id_empresa}/quantidade', [ProdutoController::class, 'findTotalProductByProvider']);
    });

    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================




    //==================================================================================================================
    //Rota para Fornecedores ===========================================================================================
    //==================================================================================================================

    Route::prefix('fornecedores')->group(function(){

        //Pegar todos os Fornecedores
        Route::get('/', [FornecedorController::class, 'index']);

        //Rota para cadastrar
        Route::get('/create', [FornecedorController::class, 'create']);

        //Rota para cadastrar um Fornecedor
        Route::post('/', [FornecedorController::class, 'store']);

        Route::get('/{id}', [FornecedorController::class, 'show']);

        Route::get('/{id}/edit', [FornecedorController::class, 'edit']);

        //Achar os fornecedores de uma empresa
        Route::get('/empresa/{idEmpresa}', [FornecedorController::class, 'providersEnterprise']);

        //Total de produtos de uma empresa
        Route::get('/quantidade/fornecedor', [FornecedorController::class, 'findTotalProvider']);

        Route::put('/{id}', [FornecedorController::class, 'update']);

        Route::delete('/{id}', [FornecedorController::class, 'destroy']);

    });

    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================


    //==================================================================================================================
    //Rota para Fornecedores ===========================================================================================
    //==================================================================================================================
    Route::prefix('historico-venda')->group(function(){

        //Pegar todos as vendas de Histórico
        Route::get('/', [HistoricoVendaController::class, 'index']);

        //Pegar todos as vendas de Histórico
        Route::post('/', [HistoricoVendaController::class, 'store']);

        Route::get('/total-amount-sell', [HistoricoVendaController::class, 'totalAmountSell']);

        Route::get('/total-sell', [HistoricoVendaController::class, 'totalSell']);

        Route::get('/total-product-sell', [HistoricoVendaController::class, 'totalProductSell']);
    });
    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================



});


//======================================================================================================================
//======================================================================================================================
//======================================================================================================================


/*Route::get('/',function(){
    echo "Bem vindo a nossa API!!";
});*/
